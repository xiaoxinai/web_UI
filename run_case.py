# -*- encoding: utf-8 -*-
"""
@User    : 24694
@File    : run_case.py.py
@Time    : 2021/8/23/023 18:24
@Author  : XXX
@Email   : liyx5343@163.com
@Software: PyCharm
"""
import sys
import subprocess

WIN = sys.platform.startswith('win')


def main():
    """主函数"""
    steps = [
        "venv\\Script\\activate" if WIN else "source venv/bin/activate",
        "pytest --alluredir allure-results --clean-alluredir",
        "allure generate allure-results -c -o allure-report",
        "allure open allure-report"
    ]
    for step in steps:
        subprocess.run("call " + step if WIN else step, shell=True)


if __name__ == "__main__":
    main()
